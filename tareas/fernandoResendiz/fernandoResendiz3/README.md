#Tarea 3.

Investiga quién está a cargo del dns que utiliza tu archivo /etc/resolv.conf.
La dirección que posee mi maquina Debian 9 es:

![Screenshot de /etc/resolv.conf](img/etc_resolv.png "Mi direccion de DNS")

Como podemos observar lo que nos provee es una dirección de DNS privada esto es debido a que mi ISP lo
que realiza es utilizar sus propios servidores de DNS por lo tanto estas direcciones no las puedo rastrear.

Busca un servidor que respete tus libertades (puntos extra si instalas tu propio servidor dns).

Mediante mi investigación he llegado a la conclusión que el DNS de OpenDNS es la opcion que respeta mis 
libertades su ip es: 208.67.222.222 y 208.67.220.220

Utiliza el servidor dns de confianza para el resto de tu vida (o el curso para fines prácticos).

![Cambio de DNS](img/modificandoDNS.png "Modificacion de DNS")

