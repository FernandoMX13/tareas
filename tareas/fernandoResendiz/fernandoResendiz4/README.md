#Tarea 2

Tomando en cuenta esta estructura organizacional:

Director: `jefe`

Subdirector: `amigo_de_jefe`

Tecnología: `nerd`

Ventas: `vendedora`

Contabilidad: `contadora`

Redes: `redes`

Programación: `programador`


Realizar una política de sudo para que se cumpla lo siguiente:

El director puede acceder a los archivos que se encuentran en /usr/share/empresa y sus archivos personales, sin embargo no puede modificar nada que se encuentre bajo /etc.
El subdirector puede acceder a los archivos que se encuentran en /usr/share/empresa/planNegocios y sus archivos personales, sin embargo no puede modificar nada que se encuentre bajo /etc. Él puede agregar nuevos usuarios y cambiar la fecha de expiración de su contraseña.
El responsable de tecnología tiene control total sobre el sistema, pero no puede ver ni realizar cambios en /usr/share/empresa.
La responsable de contabilidad tiene control total sobre la carpeta /usr/share/empresa/finanzas' y/usr/share/empresa/planNegocios`. Tiene acceso a los comandos de calendario, listas de usuario en el sistema y correo.
El responsable de redes tiene acceso a todos los comandos de redes y control sobre rchivos de configuración de la red junto al comando reboot.
El programador sólo tiene acceso a los comandos de compilación y tiene control sobre el directorio /var/www.

```

# Editar SIEMPRE con visudo!!! (visudo -c)
# Variables de ambiente SUDO_EDITOR, VISUAL, EDITOR 

# ***********************
# DEFINCION DE ALIAS
# ***********************

# Administradores de sistemas
User_Alias ADMINS = Andres, Juan, Fernando, 

# Alias del jefe
User_Alias JEFE = Director

# Alias del amigo del jefe
User_Alias AMIGO_DEL_JEFE = subdirector

# Alias del nerd
User_Alias NERD = Tecnologia

# Alias de la vendedora
User_Alias VENDEDORA = Ventas

# Alias de la contadora
User_Alias CONTADORA = Contabilidad

# Alias de REDES
User_Alias REDES = Redes

# Alias de programacion
User_Alias PROGRAMADOR = Programador

# comandos de red permitidos
Cmnd_Alias REDCMDS = /sbin/ifconfig, /sbin/iptables


# ***********************
# DEFINCION DE OPCIONES
# ***********************

# Los usuarios administradores, requieren autentificarse con la contraseña de 'root'
Defaults:ADMINS rootpw

# ***********************
# DEFINCION DE REGLAS
# ***********************

# administradores todo se les permite en cualquier equipo (¡¡¡¡¡cuidado con esto en la vida real!!!!!
ADMINS ALL = (ALL) ALL

# Reglas para el jefe
JEFE ALL = /usr/share/empresa, /home/director NOEXEC: /etc

# Reglas para el amigo del jefe
AMIGO_DEL_JEFE ALL = /usr/share/empresa/planNegocios, /home/director, /usr/sbin/useradd, /usr/sbin/adduser, /usr/bin/chage  NOEXEC: /etc

#Reglas para el Nerd
NERD ALL = ALL *, !/usr/share/empresa

#Reglas de Contabilidad.
CONTADORA ALL = /usr/share/empresa/finanzas, /usr/share/empresa/planNegocios, /usr/bin/cal, cat /etc/passwd, /usr/bin/mail

# Reglas del administrador de red.
REDES ALL = REDCMDS, /sbin/reboot

#Regla del programador.
PROGRAMADOR ALL = /usr/bin/gcc*, /usr/bin/perl*, /usr/bin/py*, /var/www


```
