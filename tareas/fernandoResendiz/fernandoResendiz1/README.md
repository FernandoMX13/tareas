#Tarea1.

Forma corta de el comando renice para cambiar la prioridad de un ping.

```
renice -n 10 -p $(ps -aux | grep ping -m 1 | cut -d" " -f4)

``
