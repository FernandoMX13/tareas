#Tarea 2

Script para crear usurios leidos desde un archivo cuyo contenido es el nombre de esto usuario separados por un coma (,)

```
#!/bin/bash

#Este programa fue desarrollado en Linux Debian 9
#Registra los usuarios que existen en un documento, estos usuarios necesitan estar separados por un ", " (coma espacio)
#Ademas genera una contraseña aleatoria asi como cambia el numero dias que es valida una contraseña.
# Ejemplo del contenido del documento:
#Hugo, Paco, Luis, Pedro, Fernando
#Ademas genera una contraseña aleatoria, no le asigna un shell y cambia el numero dias que es valida la contraseña 
# de cada usuario.
IFS=', ' read -a usu <<< "$(cat $1)"

for element in "${usu[@]}"
do	
	pass=$(dd if=/dev/urandom bs=1 count=8 2>/dev/null | base64)
	echo "$element su contraseña es: $pass"
	useradd -s /sbin/nologin -p $pass $element
	chage -M 30 $element
done

```
