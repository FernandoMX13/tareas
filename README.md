# Tareas Becarios 2019

En este repositorio tendremos las tareas.

+ Favor de revisar la [lista de issues] y el documento del [temario] para ver las tareas que se han dejado
+ Las tareas se entregan con _merge request_ a la rama `entregas`
+ Ver el repositorio [workflow] para más detalles

--------------------------------------------------------------------------------

## Método de entrega

Crear una carpeta con tu nombre dentro de la carpeta `tareas`

```
$ mkdir -vp tareas/NOMBRE
```

### Para las tareas que implican contestar preguntas

Editar el archivo `tareas/NOMBRE/README.md` y agregar las secciones adecuadas para contestar las preguntas que se piden

>>>
**ProTip**:
+ Ver el código fuente de este archivo o las guías de `Markdown`
>>>

### Para las tareas que implican crear archivos

Crear los archivos y carpetas necesarios bajo la ruta `tareas/NOMBRE`

>>>
**ProTip**:
+ Haz referencia a tus archivos en `tareas/NOMBRE/README.md`
+ Separa los archivos de cada tarea en diferentes carpetas para que sea más fácil visualizarlos
>>>

--------------------------------------------------------------------------------

## Guias de `Markdown`:

+ <https://docs.gitlab.com/ce/user/markdown.html>
+ <https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/>
+ <https://about.gitlab.com/2018/08/17/gitlab-markdown-tutorial/>
+ <https://docs.gitlab.com/ee/development/documentation/styleguide.html>

--------------------------------------------------------------------------------

[lista de issues]: https://gitlab.com/PBSC-AdminUNIX/2019/tareas/issues
[temario]: https://tinyurl.com/Temario-AdminUNIX-2019
[workflow]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/workflow
